package com.example.jajanken

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.random.Random
import kotlin.random.nextInt

class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        var yourScore = 0
        var comScore = 0
//        your Button
//        paper = 1
//        rock = 2
//        scissor = 3
        your_paper.setOnClickListener {
            val versus = Versus(1)
            val result = versus.versus(versus.your, versus.listRandom)
            changeImage(versus.your, versus.listRandom)
            changeBg(versus.result)
            tvResult.text = versus.result

            when(versus.result){
                "Win" -> yourScore++
                "Lose" -> comScore++
            }
            your_score_result.text = yourScore.toString()
            com_score_result.text = comScore.toString()
        }

        your_rock.setOnClickListener {
            val versus = Versus(2)
            val result = versus.versus(versus.your, versus.listRandom)
            changeImage(versus.your, versus.listRandom)
            changeBg(versus.result)
            tvResult.text = versus.result
            when(versus.result){
                "Win" -> yourScore++
                "Lose" -> comScore++
            }
            your_score_result.text = yourScore.toString()
            com_score_result.text = comScore.toString()
        }

        your_scissors.setOnClickListener {
            val versus = Versus(3)
            val result = versus.versus(versus.your, versus.listRandom)
            changeImage(versus.your, versus.listRandom)
            changeBg(versus.result)
            tvResult.text = versus.result
            when(versus.result){
                "Win" -> yourScore++
                "Lose" -> comScore++
            }
            your_score_result.text = yourScore.toString()
            com_score_result.text = comScore.toString()
        }


        btn_reset.setOnClickListener {
            your_Choose.setImageResource(android.R.color.transparent)
            com_Choose.setImageResource(android.R.color.transparent)
            tvResult.text = ""
            your_score_result.text = "0"
            com_score_result.text = "0"
            yourScore = 0
            comScore = 0
            bg_result.setImageResource(R.drawable.ic_bg_win_foreground)
        }

    }

    fun changeImage(your:Int, com: Int) {
        when(your){
            1 -> your_Choose.setImageResource(R.drawable.paper)
            2 -> your_Choose.setImageResource(R.drawable.rock)
            3 -> your_Choose.setImageResource(R.drawable.scissors)
        }

        when(com){
            1 -> com_Choose.setImageResource(R.drawable.paper)
            2 -> com_Choose.setImageResource(R.drawable.rock)
            3 -> com_Choose.setImageResource(R.drawable.scissors)
        }
    }

    fun changeBg(result: String) {
        when(result){
            "Win" -> bg_result.setImageResource(R.drawable.ic_bg_win_foreground)
            "Draw" -> bg_result.setImageResource(R.drawable.ic_bg_draw_foreground)
            "Lose" -> bg_result.setImageResource(R.drawable.ic_bg_lose_foreground)
        }
    }
}


class Versus(val your : Int){
    val listRandom = Random.nextInt(1..3)
    var result = ""
    fun versus(your: Int, com: Int): String {
        if (your == com) {
            result = "Draw"
        } else {
            when (your) {
                1 -> {
                    if (com == 2) {
                        result = "Win"
                    }
                    if (com == 3) {
                        result = "Lose"
                    }
                }
                2 -> {
                    if (com == 3) {
                        result = "Win"
                    }
                    if (com == 1) {
                        result = "Lose"
                    }
                }
                3 -> {
                    if (com == 1) {
                        result = "Win"
                    }
                    if (com == 2) {
                        result = "Lose"
                    }
                }
            }
        }
        return result
    }
}